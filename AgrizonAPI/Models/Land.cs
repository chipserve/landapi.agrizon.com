﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AgrizonAPI.Models
{
    public class Land
    {
        public int landId { get; set; }
        public string dtmAdd { get; set; }
        public string dtmUpdate { get; set; }
        public string isValid { get; set; }
        public string isStatus { get; set; }
        public int userId { get; set; }
        public int profileId { get; set; }
        public int memberId { get; set; }
        public string landRecordNo1 { get; set; }
        public string landRecordNo2 { get; set; }
        public string plotSize { get; set; }
        public string unit { get; set; }
        public string soilType { get; set; }
        public string lat { get; set; }
        public string longs { get; set; }
        public string polygon { get; set; }
        public string utilization { get; set; }
        public string district { get; set; }
        public string village { get; set; }
        public string state { get; set; }
        public string pincode { get; set; }
        public string registrarOffice { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class CreateLand:Land
    {

    }

    public class ReadLand:Land
    {
        public ReadLand(DataRow dr)
        {
            landId = Convert.ToInt32(dr["landId"]);
            dtmAdd = dr["dtmAdd"].ToString();
            dtmUpdate = dr["dtmUpdate"].ToString();
            isValid = dr["isValid"].ToString();
            isStatus = dr["isStatus"].ToString();
            profileId = Convert.ToInt32(dr["profileId"]);
            userId = Convert.ToInt32(dr["userId"]);
            memberId = Convert.ToInt32(dr["memberId"]);
            landRecordNo1 = dr["landRecordNo1"].ToString();
            landRecordNo2 = dr["landRecordNo2"].ToString();
            plotSize = dr["plotSize"].ToString();
            unit = dr["unit"].ToString();
            soilType = dr["soilType"].ToString();
            lat = dr["lat"].ToString();
            longs = dr["longs"].ToString();
            polygon = dr["polygon"].ToString();
            utilization = dr["utilization"].ToString();
            district = dr["district"].ToString();
            village = dr["village"].ToString();
            state = dr["state"].ToString();
            pincode = dr["pincode"].ToString();
            registrarOffice = dr["registrarOffice"].ToString();
            ErrorMessage = "1";
        }
    }
   
    public class ReadMess:Land
    {
        public ReadMess(string mess)
        {
            ErrorMessage = mess.ToString();
        }
    }
}