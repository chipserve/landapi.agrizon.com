﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AgrizonAPI.Models
{
    public class Attachment
    {
        public int AttachmentId { get; set; }
        public string dtmAdd { get; set; }
        public string dtmUpdate { get; set; }
        public string isValid { get; set; }
        public int userId { get; set; }
        public int landId { get; set; }
        public int cropId { get; set; }
        public string type { get; set; }
        public string title { get; set; }
        public string desciption { get; set; }
        public string path { get; set; }
        public string AttachmentName { get; set; }
    }

    public class CreateAttachment : Attachment
    {
    }

    public class ReadAttachment : Attachment
    {
        public ReadAttachment(DataRow dr)
        {
            AttachmentId = Convert.ToInt32(dr["AttachmentId"]);
            dtmAdd = dr["dtmAdd"].ToString();
            dtmUpdate = dr["dtmUpdate"].ToString();
            isValid = dr["isValid"].ToString();
            userId = Convert.ToInt32(dr["userId"]);
            landId = Convert.ToInt32(dr["landId"]);
            cropId = Convert.ToInt32(dr["cropId"]);
            type = dr["type"].ToString();
            title = dr["title"].ToString();
            desciption = dr["desciption"].ToString();
            path = dr["path"].ToString();
            AttachmentName = dr["AttachmentName"].ToString();
        }
    }
}