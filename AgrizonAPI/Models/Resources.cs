﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AgrizonAPI.Models
{
    public class Resources
    {
        public int resourcesId { get; set; }
        public string dtmAdd { get; set; }
        public string dtmUpdate { get; set; }
        public string isValid { get; set; }
        public string isStatus { get; set; }
        public string userId { get; set; }
        public string profileId { get; set; }
        public string memberId { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string State { get; set; }
        public string Rate { get; set; }
    }

    public class CreateResources: Resources
    {

    }

    public class ReadResources : Resources
    {
        public ReadResources(DataRow dr)
        {
            resourcesId = Convert.ToInt32(dr["resourcesId"]);
            dtmAdd = dr["dtmAdd"].ToString();
            dtmUpdate = dr["dtmUpdate"].ToString();
            isValid = dr["isValid"].ToString();
            isStatus = dr["isStatus"].ToString();
            userId = dr["userId"].ToString();
            profileId = dr["profileId"].ToString();
            memberId = dr["memberId"].ToString();
            Type = dr["Type"].ToString();
            Description = dr["Description"].ToString();
            State = dr["State"].ToString();
            Rate = dr["Rate"].ToString();

        }
    }
}