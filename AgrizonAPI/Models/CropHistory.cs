﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AgrizonAPI.Models
{
    public class CropHistory
    {
        public int cropHistoryId { get; set; }
        public string dtmAdd { get; set; }
        public string dtmUpdate { get; set; }
        public string isValid { get; set; }
        public string isStatus { get; set; }
        public string userId { get; set; }
        public string landId { get; set; }
        public string cropId { get; set; }
        public string cropName { get; set; }
        public string cropType { get; set; }
        public string cropCategory { get; set; }
        public string cropYear { get; set; }
        public string cropPeriod { get; set; }
        public string cropProduction { get; set; }
        public string cropValue { get; set; }
        public string cropdistrict { get; set; }
        public string cropVillage { get; set; }
        public string cropState { get; set; }
        public string cropInsurance { get; set; }
        public string cropStatus { get; set; }

    }

    public class CreateCropHistory: CropHistory
    {

    }

    public class ReadCropHistory: CropHistory
    {
        public ReadCropHistory(DataRow dr)
        {
            cropHistoryId = Convert.ToInt32(dr["cropHistoryId"]);
            dtmAdd = dr["dtmAdd"].ToString();
            dtmUpdate = dr["dtmUpdate"].ToString();
            isValid = dr["isValid"].ToString();
            isStatus = dr["isStatus"].ToString();
            userId = dr["userId"].ToString();
            landId = dr["landId"].ToString();
            cropId = dr["cropId"].ToString();
            cropName = dr["cropName"].ToString();
            cropType = dr["cropType"].ToString();
            cropCategory = dr["cropCategory"].ToString();
            cropYear = dr["cropYear"].ToString();
            cropPeriod = dr["cropPeriod"].ToString();
            cropProduction = dr["cropProduction"].ToString();
            cropValue = dr["cropValue"].ToString();
            cropdistrict = dr["cropdistrict"].ToString();
            cropVillage = dr["cropVillage"].ToString();
            cropState = dr["cropState"].ToString();
            cropInsurance = dr["cropInsurance"].ToString();
            cropStatus = dr["cropStatus"].ToString();

        }
    }
}