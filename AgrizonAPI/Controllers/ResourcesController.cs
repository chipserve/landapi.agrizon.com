﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AgrizonAPI.Models;

namespace AgrizonAPI.Controllers
{
    public class ResourcesController : ApiController
    {
        // GET: Resources
        public IEnumerable<Resources> Get()
        {
            string query = "select * from tbl.Land";

            DataTable dt = Database.get_DataTable(query);
            List<Resources> resources = new List<Models.Resources>(dt.Rows.Count);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    resources.Add(new ReadResources(dr));
                }
            }
            return resources;
        }

        public IEnumerable<Resources> Get(int id)
        {
            string query = "select * from tbl.Land where userid='" + id + "''";

            DataTable dt = Database.get_DataTable(query);
            List<Resources> resources = new List<Models.Resources>(dt.Rows.Count);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    resources.Add(new ReadResources(dr));
                }
            }
            return resources;
        }

        public string Post([FromBody]CreateResources values)
        {
            try
            {
                string query = "insert into tbl.Land(dtmAdd,dtmUpdate,isValid,isStatus,profileId,userId,memberId,Type,Description,State," +
                    "Rate) values ('" + DateTime.Now.ToString()
                        + "','" + DateTime.Now.ToString() + "','1','1','" + values.profileId + "','" + values.userId + "','" + values.memberId + "','" + values.Type
                        + "','" + values.Description + "','" + values.State + "','" + values.Rate + "')";
                int res = Database.Execute(query);
                if (res == 1)
                {                    
                    return "true";
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }

        public string Put(int id, [FromBody]CreateResources value)
        {
            string query = "update tbl.Land set dtmUpdate='" + value.dtmUpdate + "', isValid='" + value.isValid + "',isStatus='" + value.isStatus
                + "',Type='" + value.Type + "',Description='" + value.Description + "',Rate='" + value.Rate
                + "' where resourcesId='" + id + "'";
            int res = Database.Execute(query);
            if (res == 1)
            {
                return "true";
            }
            else
            {
                return "false";
            }
        }
    }
}