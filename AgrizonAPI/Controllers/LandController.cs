﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AgrizonAPI.Models;

namespace AgrizonAPI.Controllers
{
    public class LandController : ApiController
    {
        // GET: Land
        public IEnumerable<Land> Get()
        {
            string query = "select * from tbl.Land";

            DataTable dt = Database.get_DataTable(query);
            List<Land> profile = new List<Models.Land>(dt.Rows.Count);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    profile.Add(new ReadLand(dr));
                }
            }
            return profile;
        }

        public IEnumerable<Land> Get(int id)
        {
            List<Land> profile = new List<Models.Land>();
            try
            {
                string query = "select * from tbl.Land where userid='" + id + "'";
                DataTable dt = Database.get_DataTable(query);
                
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        profile.Add(new ReadLand(dr));
                    }
                }
                return profile;
            }
           catch(Exception ex)
            {

                profile.Add(new ReadMess("0"));
                return profile;
            }
        }


        public IEnumerable<Land> GetLand(int userId,int landId)
        {
            List<Land> profile = new List<Models.Land>();
            try
            {
                string query = "select * from tbl.Land where userid='" + userId + "' and landId='"+ landId + "'";
                DataTable dt = Database.get_DataTable(query);

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        profile.Add(new ReadLand(dr));
                    }
                }
                return profile;
            }
            catch (Exception ex)
            {

                profile.Add(new ReadMess("0"));
                return profile;
            }
        }

        public string Post([FromBody]CreateLand values)
        {
            try
            {
                string sqlQuery = "", sqlQueryVal = "";                

                if (values.landRecordNo1 != null)
                {
                    sqlQuery += "landRecordNo1,";
                    sqlQueryVal += "'" + values.landRecordNo1 + "',";
                }

                if (values.landRecordNo2 != null)
                {
                    sqlQuery += "landRecordNo2,";
                    sqlQueryVal += "'" + values.landRecordNo2 + "',";
                }

                if (values.plotSize != null)
                {
                    sqlQuery += "plotSize,";
                    sqlQueryVal += "'" + values.plotSize + "',";
                }

                if (values.unit != null)
                {
                    sqlQuery += "unit,";
                    sqlQueryVal += "'" + values.unit + "',";
                }

                if (values.soilType != null)
                {
                    sqlQuery += "soilType,";
                    sqlQueryVal += "'" + values.soilType + "',";
                }

                if (values.lat != null)
                {
                    sqlQuery += "lat,";
                    sqlQueryVal += "'" + values.lat + "',";
                }

                if (values.longs != null)
                {
                    sqlQuery += "longs,";
                    sqlQueryVal += "'" + values.longs + "',";
                }

                if (values.polygon != null)
                {
                    sqlQuery += "polygon,";
                    sqlQueryVal+= "'" + values.polygon + "',";
                }

                if (values.utilization != null)
                {
                    sqlQuery += "utilization,";
                    sqlQueryVal += "'" + values.utilization + "',";
                }

                if (values.district != null)
                {
                    sqlQuery += "district,";
                    sqlQueryVal += "'" + values.district + "',";
                }

                if (values.village != null)
                {
                    sqlQuery += "village,";
                    sqlQueryVal += "'" + values.village + "',";
                }

                if (values.state != null)
                {
                    sqlQuery += "state,";
                    sqlQueryVal += "'" + values.state + "',";
                }

                if (values.pincode != null)
                {
                    sqlQuery += "pincode,";
                    sqlQueryVal += "'" + values.pincode + "',";
                }

                if (values.registrarOffice != null)
                {
                    sqlQuery += "registrarOffice,";
                    sqlQueryVal += "'" + values.registrarOffice + "',";
                }

                string query = "insert into tbl.Land(dtmAdd,dtmUpdate," + sqlQuery + " profileId,userId,memberId) values ('" + DateTime.Now.ToString()
                        + "','" + DateTime.Now.ToString() + "'," + sqlQueryVal + " '" + values.profileId + "','" + values.userId
                        + "','" + values.memberId + "')";
                int res = Database.Execute(query);
                if (res == 1)
                {
                    string LandId = "";
                    string queryLand = "select top 1 landId from tbl.land where userid='" + values.userId + "' order by landId desc";
                    DataSet dsLand = Database.get_DataSet(queryLand);
                    if(dsLand.Tables[0].Rows.Count>0)
                    {
                        LandId = dsLand.Tables[0].Rows[0]["landId"].ToString();
                    }
                    return "1-" + LandId;
                }
                else
                {
                    return "0";
                }
            }
            catch (Exception ex)
            {               
                return "-1";
            }
        }

        public string Put(int id, [FromBody]CreateLand value)
        {
            string sqlQuery = "";
            if(value.isValid!=null)
            {
                sqlQuery += "isValid='" + value.isValid + "',";
            }

            if(value.isStatus!=null)
            {
                sqlQuery += "isStatus = '" + value.isStatus + "',";
            }

            if (value.landRecordNo1 != null)
            {
                sqlQuery += "landRecordNo1='" + value.landRecordNo1 + "',";
            }

            if (value.landRecordNo2 != null)
            {
                sqlQuery += "landRecordNo2='" + value.landRecordNo2 + "',";
            }

            if (value.plotSize != null)
            {
                sqlQuery += "plotSize='" + value.plotSize + "',";
            }

            if (value.unit != null)
            {
                sqlQuery += "unit='" + value.unit + "',";
            }

            if(value.soilType!=null)
            {
                sqlQuery += "soilType='" + value.soilType + "',";
            }

            if (value.lat != null)
            {
                sqlQuery += "lat='" + value.lat + "',";
            }

            if (value.longs != null)
            {
                sqlQuery += "longs='" + value.longs + "',";
            }

            if (value.polygon != null)
            {
                sqlQuery += "polygon='" + value.polygon + "',";
            }

            if (value.utilization != null)
            {
                sqlQuery += "utilization='" + value.utilization + "',";
            }

            if (value.district != null)
            {
                sqlQuery += "district='" + value.district + "',";
            }

            if (value.village != null)
            {
                sqlQuery += "village='" + value.village + "',";
            }

            if (value.state != null)
            {
                sqlQuery += "state='" + value.state + "',";
            }

            if (value.pincode != null)
            {
                sqlQuery += "pincode='" + value.pincode + "',";
            }

            if (value.registrarOffice != null)
            {
                sqlQuery += "registrarOffice='" + value.registrarOffice + "',";
            }

            string query = "update tbl.Land set " + sqlQuery + " dtmUpdate='" + DateTime.Now.ToString() + "'  where landId='" + id + "'";
            int res = Database.Execute(query);
            if (res == 1)
            {
                return "1";
            }
            else
            {
                return "0";
            }
        }
    }
}