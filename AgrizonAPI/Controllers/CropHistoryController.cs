﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AgrizonAPI.Models;

namespace AgrizonAPI.Controllers
{
    public class CropHistoryController : ApiController
    {
        // GET: CropHistory
        public IEnumerable<CropHistory> Get()
        {
            string query = "select * from tbl.CropHistory";
            DataTable dt = Database.get_DataTable(query);
            List<CropHistory> cropHistory = new List<Models.CropHistory>(dt.Rows.Count);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    cropHistory.Add(new ReadCropHistory(dr));
                }
            }
            return cropHistory;
        }

        //public IEnumerable<CropHistory> GetData(int val,int id)
        //{
        //    //string[] param = val.Split('|');

        //    string query = "select * from tbl.CropHistory where userid='1'";

        //    DataTable dt = Database.get_DataTable(query);
        //    List<CropHistory> cropHistory = new List<Models.CropHistory>(dt.Rows.Count);
        //    if (dt.Rows.Count > 0)
        //    {
        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            cropHistory.Add(new ReadCropHistory(dr));
        //        }
        //    }
        //    return cropHistory;
        //}

        public IEnumerable<CropHistory> GetCropHistory(int userId, int landId, int cropId)
        {
            string query = "";
            if (userId != 0)
            {
                query = "select * from tbl.CropHistory where userid='" + userId + "'";

                if (landId != 0)
                {
                    query = "select * from tbl.CropHistory where userid='" + userId + "' and landId='" + landId + "'";

                    if (cropId != 0)
                    {
                        query = "select * from tbl.CropHistory where userid='" + userId + "' and landId='" + landId + "' and cropId='" + cropId + "'";
                    }
                }                
            }          

            DataTable dt = Database.get_DataTable(query);
            List<CropHistory> cropHistory = new List<Models.CropHistory>(dt.Rows.Count);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    cropHistory.Add(new ReadCropHistory(dr));
                }
            }
            return cropHistory;
        }

        public string Post([FromBody]CreateCropHistory values)
        {
            try
            {
                string sqlQuery = "", sqlQueryVal = "";
                if(values.userId!=null)
                {
                    sqlQuery += "userId,";
                    sqlQueryVal += "'" + values.userId + "',";
                }

                if (values.landId != null)
                {
                    sqlQuery += "landId,";
                    sqlQueryVal += "'" + values.landId + "',";
                }

                if (values.cropId != null)
                {
                    sqlQuery += "cropId,";
                    sqlQueryVal += "'" + values.cropId + "',";
                }
                
                if (values.cropName != null)
                {
                    sqlQuery += "cropName,";
                    sqlQueryVal += "'" + values.cropName + "',";
                }

                if (values.cropType != null)
                {
                    sqlQuery += "cropType,";
                    sqlQueryVal += "'" + values.cropType + "',";
                }

                if (values.cropCategory != null)
                {
                    sqlQuery += "cropCategory,";
                    sqlQueryVal += "'" + values.cropCategory + "',";
                }

                if (values.cropYear != null)
                {
                    sqlQuery += "cropYear,";
                    sqlQueryVal += "'" + values.cropYear + "',";
                }

                if (values.cropPeriod != null)
                {
                    sqlQuery += "cropPeriod,";
                    sqlQueryVal += "'" + values.cropPeriod + "',";
                }

                if (values.cropProduction != null)
                {
                    sqlQuery += "cropProduction,";
                    sqlQueryVal += "'" + values.cropProduction + "',";
                }

                if (values.cropProduction != null)
                {
                    sqlQuery += "cropProduction,";
                    sqlQueryVal += "'" + values.cropProduction + "',";
                }

                if (values.cropValue != null)
                {
                    sqlQuery += "cropValue,";
                    sqlQueryVal += "'" + values.cropValue + "',";
                }

                if (values.cropdistrict != null)
                {
                    sqlQuery += "cropdistrict,";
                    sqlQueryVal += "'" + values.cropdistrict + "',";
                }

                if (values.cropVillage != null)
                {
                    sqlQuery += "cropVillage,";
                    sqlQueryVal += "'" + values.cropVillage + "',";
                }

                if (values.cropState != null)
                {
                    sqlQuery += "cropState,";
                    sqlQueryVal += "'" + values.cropState + "',";
                }

                if (values.cropInsurance != null)
                {
                    sqlQuery += "cropInsurance,";
                    sqlQueryVal += "'" + values.cropInsurance + "',";
                }

                if (values.cropStatus != null)
                {
                    sqlQuery += "cropStatus,";
                    sqlQueryVal += "'" + values.cropStatus + "',";
                }

                string query = "insert into tbl.CropHistory(dtmAdd,dtmUpdate," + sqlQuery + "isValid,isStatus) values ('" + DateTime.Now.ToString()
                    + "','" + DateTime.Now.ToString() + "'," + sqlQueryVal + "'1','1',)";
                int res = Database.Execute(query);
                if (res == 1)
                {
                    return "1";
                }
                else
                {
                    return "0";
                }
            }
            catch (Exception ex)
            {
                return "-1";
            }
        }

        public string Put(int id, [FromBody]CreateCropHistory value)
        {
            string sqlQuery = "";
            if (value.cropName != null)
            {
                sqlQuery += "cropName='" + value.cropName + "',";
            }

            if (value.cropType != null)
            {
                sqlQuery += "cropType='" + value.cropType + "',";
            }

            if (value.cropCategory != null)
            {
                sqlQuery += "cropCategory='" + value.cropCategory + "',";
            }

            if (value.cropYear != null)
            {
                sqlQuery += "cropYear='" + value.cropYear + "',";
            }

            if (value.cropPeriod != null)
            {
                sqlQuery += "cropPeriod='" + value.cropPeriod + "',";
            }

            if (value.cropProduction != null)
            {
                sqlQuery += "cropProduction='" + value.cropProduction + "',";
            }

            if (value.cropValue != null)
            {
                sqlQuery += "cropValue='" + value.cropValue + "',";
            }

            if (value.cropdistrict != null)
            {
                sqlQuery += "cropdistrict='" + value.cropdistrict + "',";
            }

            if (value.cropVillage != null)
            {
                sqlQuery += "cropVillage='" + value.cropVillage + "',";
            }

            if (value.cropState != null)
            {
                sqlQuery += "cropState='" + value.cropState + "',";
            }

            if (value.cropInsurance != null)
            {
                sqlQuery += "cropInsurance='" + value.cropInsurance + "',";
            }

            if (value.cropInsurance != null)
            {
                sqlQuery += "cropStatus='" + value.cropStatus + "',";
            }

            string query = "update tbl.CropHistory set dtmUpdate='" + value.dtmUpdate + "', isStatus='" + value.isStatus
                + ",'" + sqlQuery + " isValid='" + value.isValid + "' where cropHistoryId='" + id + "'";
            int res = Database.Execute(query);
            if (res == 1)
            {
                return "true";
            }
            else
            {
                return "false";
            }
        }
    }
}