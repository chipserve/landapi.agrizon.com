﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AgrizonAPI.Models;


namespace AgrizonAPI.Controllers
{
    public class AttachmentController : ApiController
    {

        // GET api/documentation
        /// <summary>
        /// Pass userId of login User
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Attachment> Get(string id)
        {
            string query = "select * from tbl.Attachment where userId='" + id + "'";

            DataTable dt = Database.get_DataTable(query);
            List<Attachment> profile = new List<Models.Attachment>(dt.Rows.Count);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    profile.Add(new ReadAttachment(dr));
                }
            }
            return profile;
        }


        public IEnumerable<Attachment> GetAttachment(int userId, int landId, int cropId)
        {
            string query = "";
            if (userId != 0)
            {
                query = "select * from tbl.Attachment where userid='" + userId + "'";

                if (landId != 0)
                {
                    query = "select * from tbl.Attachment where userid='" + userId + "' and landId='" + landId + "'";

                    if (cropId != 0)
                    {
                        query = "select * from tbl.Attachment where userid='" + userId + "' and landId='" + landId + "' and cropId='" + cropId + "'";
                    }
                }
            }

            DataTable dt = Database.get_DataTable(query);
            List<Attachment> Attachment = new List<Models.Attachment>(dt.Rows.Count);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    Attachment.Add(new ReadAttachment(dr));
                }
            }
            return Attachment;
        }

        public string Post([FromBody]CreateAttachment values)
        {
            try
            {
                HttpResponseMessage result = null;
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {
                    var docfiles = new List<string>();
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        var filePath = HttpContext.Current.Server.MapPath("~/Upload/" + postedFile.FileName);
                        postedFile.SaveAs(filePath);
                        docfiles.Add(filePath);
                    }
                    result = Request.CreateResponse(HttpStatusCode.Created, docfiles);
                }
                else
                {
                    result = Request.CreateResponse(HttpStatusCode.BadRequest);
                }

                string AId = "";
                string query_attachment = "insert into tbl.Attachment(dtmAdd,dtmUpdate,isValid,userId,type,title,desciption,path,AttachmentName,landId,cropId) values ('" + DateTime.Now.ToString()
                        + "','" + DateTime.Now.ToString() + "','1','" + values.userId + "','" + values.type + "','" + values.title + "','" + values.desciption + "','" + result
                        + "','" + values.AttachmentName + "','" + values.landId + "','" + values.cropId + "')";
                int res = Database.Execute(query_attachment);
                if (res == 1)
                {
                    string query_id = "declare @AttachmentId bigint select @AttachmentId=IDENT_CURRENT('tbl.Attachment') " +
                        "select AttachmentId from tbl.Attachment where AttachmentId=@AttachmentId";
                    DataSet ds_id = Database.get_DataSet(query_id);
                    if(ds_id.Tables[0].Rows.Count>0)
                    {
                        AId = ds_id.Tables[0].Rows[0]["AttachmentId"].ToString();
                    }
                    return "1-" + AId;
                }
                else
                {
                    return "0";
                }
            }
            catch (Exception ex)
            {
                return "-1";
            }

        }
    }
}